# Find Log4cxx library.
#
# Set LOG4CXX_ROOT to provide a hint as to where the files are found.
# It is expected that the structure looks something _like_ this,
# although the include/ and lib/ directories are optional.
#
#    include/
#       log4cxx/
#           log4cxx.h
#           ...
#    lib/
#       liblog4cxx.a
#
# Once done this will define:
#    LOG4CXX_FOUND         - System has Log4cxx
#    LOG4CXX_INCLUDE_DIRS  - The Log4cxx include directories
#    LOG4CXX_LIBRARIES     - The libraries needed to use Log4cxx

find_path(LOG4CXX_INCLUDE_DIR log4cxx/log4cxx.h
    HINTS
    ${LOG4CXX_ROOT}
    PATH_SUFFIXES include
)

find_library(LOG4CXX_LIBRARY NAMES log4cxx
    HINTS
    ${LOG4CXX_ROOT}
    PATH_SUFFIXES lib bin
)

find_library(LOG4CXX_LIBRARY_DEBUG NAMES log4cxxd
    HINTS
    ${LOG4CXX_ROOT}
    PATH_SUFFIXES lib bin
)
if(NOT LOG4CXX_LIBRARY_DEBUG)
    message("Could not find debug log4cxx library.")
    if(UNIX)
        message("Setting debug log4cxx library to same as optimized.")
        set(LOG4CXX_LIBRARY_DEBUG ${LOG4CXX_LIBRARY})
    endif()
endif()

set(LOG4CXX_LIBRARIES optimized ${LOG4CXX_LIBRARY} debug ${LOG4CXX_LIBRARY_DEBUG})
set(LOG4CXX_INCLUDE_DIRS ${LOG4CXX_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Log4cxx DEFAULT_MSG LOG4CXX_LIBRARY LOG4CXX_INCLUDE_DIR)

mark_as_advanced(LOG4CXX_INCLUDE_DIR LOG4CXX_LIBRARY LOG4CXX_LIBRARY_DEBUG)
