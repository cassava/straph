# Find Straph library.
#
# Set STRAPH_ROOT to provide a hint as to where the files are found.
# It is expected that the structure looks something _like_ this,
# although the include/ and lib/ directories are optional.
#
#    include/
#       straph/
#           straph.h
#           ...
#    lib/
#       libstraph.a
#
# Once done this will define:
#    STRAPH_FOUND         - System has Straph
#    STRAPH_INCLUDE_DIRS  - The Straph include directories
#    STRAPH_LIBRARIES     - The libraries needed to use Straph

find_path(STRAPH_INCLUDE_DIR straph/straph.h
    HINTS
    ${STRAPH_ROOT}
    PATH_SUFFIXES include
)

find_library(STRAPH_LIBRARY NAMES straph
    HINTS
    ${STRAPH_ROOT}
    PATH_SUFFIXES lib bin
)

find_library(STRAPH_LIBRARY_DEBUG NAMES straphd
    HINTS
    ${STRAPH_ROOT}
    PATH_SUFFIXES lib bin
)
if(NOT STRAPH_LIBRARY_DEBUG)
    message("Could not find debug log4cxx library.")
    if(UNIX)
        message("Setting debug log4cxx library to same as optimized.")
        set(STRAPH_LIBRARY_DEBUG ${STRAPH_LIBRARY})
    endif()
endif()

set(STRAPH_LIBRARIES optimized ${STRAPH_LIBRARY} debug ${STRAPH_LIBRARY_DEBUG})
set(STRAPH_INCLUDE_DIRS ${STRAPH_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Straph DEFAULT_MSG STRAPH_LIBRARY STRAPH_INCLUDE_DIR)

mark_as_advanced(STRAPH_INCLUDE_DIR STRAPH_LIBRARY STRAPH_LIBRARY_DEBUG)
