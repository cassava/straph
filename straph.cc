/*
 * straph.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 02. December 2014
 */

#include "straph.h"

#include <cassert>
#include <cstdio>
#include <limits>
#include <map>
#include <ostream>
#include <utility>
using namespace std;

#include <boost/graph/graph_traits.hpp>
#include <log4cxx/logger.h>
#include <libshp/shapefil.h>

namespace straph {

/*
 * log shall be available for the entire library.
 * There are a few logging macros available to us now:
 *
 *  TRACE, DEBUG, INFO, WARN, ERROR and FATAL
 *
 * We can use these then like so:
 *
 *  LOG4CXX_DEBUG(log, "starting string" << "further objects")
 *
 * Note that there is no semi-colon at the end, but you can add one.
 *
 */
log4cxx::LoggerPtr log = log4cxx::Logger::getLogger("straph");

std::size_t Junction::count = 0;
std::size_t Segment::count = 0;

void normalize_straph(Straph& graph)
{
    typedef boost::graph_traits<Straph> traits;
    Point min(numeric_limits<double>::max(), numeric_limits<double>::max());

    // Find the minimum point
    traits::edge_iterator ei, ee;
    for (tie(ei, ee) = boost::edges(graph); ei != ee; ++ei) {
        for (const Point& p : graph[*ei].polyline) {
            if (p.x < min.x) {
                min.x = p.x;
            }
            if (p.y < min.y) {
                min.y = p.y;
            }
        }
    }

    // Subtract from all values the minimum
    for (tie(ei, ee) = boost::edges(graph); ei != ee; ++ei) {
        for (Point& p : graph[*ei].polyline) {
            p -= min;
        }
    }

    traits::vertex_iterator vi, ve;
    for (tie(vi, ve) = boost::vertices(graph); vi != ve; ++vi) {
        graph[*vi].coord -= min;
    }
}

void scale_straph(Straph& graph, double scalar)
{
    typedef boost::graph_traits<Straph> traits;

    traits::vertex_iterator vi, ve;
    for (tie(vi, ve) = boost::vertices(graph); vi != ve; ++vi) {
        graph[*vi].scale(scalar);
    }

    traits::edge_iterator ei, ee;
    for (tie(ei, ee) = boost::edges(graph); ei != ee; ++ei) {
        graph[*ei].scale(scalar);
    }
}

std::ostream& operator<<(std::ostream& out, const Straph& graph)
{
    typedef boost::graph_traits<Straph> traits;

    out << "{" << endl;

    // Edges
    traits::edge_iterator ei, ee;
    out << "\tEdges: [" << endl;
    for (tie(ei, ee) = boost::edges(graph); ei != ee; ++ei) {
        out << "\t\t" << graph[*ei] << endl;
    }
    out << "\t]" << endl;

    // Vertices
    traits::vertex_iterator vi, ve;
    out << "\tVertices: [" << endl;
    for (tie(vi, ve) = boost::vertices(graph); vi != ve; ++vi) {
        out << "\t\t" << graph[*vi] << ": ";
        traits::out_edge_iterator ei, ee;
        bool first = true;
        for (tie(ei, ee) = boost::out_edges(*vi, graph); ei != ee; ++ei) {
            if (first) {
                out << ", ";
                first = false;
            }
            out << graph[*ei].index;
        }
        out << endl;
    }
    out << "\t]" << endl;
    out << "}" << endl;
    return out;
}

bool route_valid(const Route& route)
{
    for (const RouteSegment& rs : route) {
        // There shouldn't be any empty route segments, and the name should not
        // be empty.
        if (rs.segments.empty() || rs.name == "") {
            return false;
        }
        for (const Segment& s : rs.segments) {
            // There also shouldn't be any segments with less than 2 points,
            // and the name of the segment should equal that of the route
            // segment.
            if (s.polyline.size()<2 || s.name != rs.name) {
                return false;
            }
        }
    }
    return true;
}

namespace {

bool is_unknown(const std::string& s)
{
    return s.find("Unknown") == 0;
}

}

void route_rename(Route& route, bool number)
{
    map<string,int> names;
    string next;

    // Look ahead to find the first valid name.
    for (RouteSegment& rs : route) {
        if (!is_unknown(rs.name)) {
            next = rs.name;
            names[rs.name] = 1;
            break;
        }
    }

    // If there is no valid name in the whole set, abort.
    if (next.empty()) {
        return;
    }

    // Use a function to handle whether we number or not.
    auto genname = [&] (bool next_unknown) -> string {
        if (number) {
            // If this is the first instance, and the next route segment is not unknown,
            // then don't append a number.
            if (names[next] == 1 && !next_unknown) {
                return next;
            }

            stringstream ss;
            ss << next << "-" << names[next];
            names[next]++;
            return ss.str();
        }
        return next;
    };

    // Rename all route segments.
    for (size_t i = 0; i < route.size(); i++) {
        if (!is_unknown(route[i].name)) {
            next = route[i].name;
            if (names.count(next) == 0) {
                names[next] = 1;
            }
        }

        bool u = false;
        if (i+1 < route.size()) {
            u = is_unknown(route[i+1].name);
        }
        route[i].set_name(genname(u));
    }
}

std::unique_ptr<Route> combine_routes(const std::vector<Route*>& list)
{
    unique_ptr<Route> rte(new Route);
    for (const Route* r : list) {
        if (!rte->empty()) {
            Point last = rte->back().segments.back().back();
            Point next = r->front().segments.front().front();
            if (last != next) {
                LOG4CXX_ERROR(log, "Last point in route is not same as next point.")
                return nullptr;
            }
        }

        rte->insert(rte->end(), r->begin(), r->end());
    }
    return rte;
}

std::vector<Polyline> get_route_polylines(const Route& route)
{
    vector<Polyline> polylines;
    // TODO: Prevent polylines from being in here double.
    for (const RouteSegment& rs : route) {
        for (const Segment& s : rs.segments) {
            polylines.push_back(s.polyline);
        }
    }
    return polylines;
}

std::vector<Junction> get_route_junctions(const Straph& graph, const Route& route)
{
    auto find_junction = [&] (Edge e, Point ref) -> Junction {
        Junction j = graph[source(e, graph)];
        if (j.coord == ref) {
            return j;
        } else {
            return graph[target(e, graph)];
        }
    };

    vector<Junction> js;
    for (const RouteSegment& rs : route) {
        Edge e = rs.edges.front();
        Point p = rs.segments.front().front();
        Junction j = find_junction(e, p);
        js.push_back(j);
    }
    Edge e = route.back().edges.back();
    Point p = route.back().segments.back().back();
    js.push_back(find_junction(e, p));
    return js;
}

std::ostream& operator<<(std::ostream& out, const RouteSegment& r)
{
    out << "{" << endl;
    out << "\t" << r.name << endl;
    for (const Segment& s : r.segments) {
        out << "\t" << s.index << ": " << s.polyline << endl;
    }
    out << "}" << endl;
    return out;
}

std::ostream& operator<<(std::ostream& out, const Route& r)
{
    out << "{" << endl;
    for (const RouteSegment& rs : r) {
        out << "\t" << rs.name << " {" << endl;
        for (const Segment& s : rs.segments) {
            out << "\t\t" << s.index << ": " << s.polyline << endl;
        }
        out << "\t}" << endl;
    }
    out << "}" << endl;
    return out;
}

} // namespace straph
