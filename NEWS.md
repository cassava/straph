Releases
========

## Version 1.4 (23 February 2015)
This release fixes shapefile reading to match Dennis' code better.
We ignore all routes (with proper=true) without a name now.
Additionally, we ignore the `name_idx` parameter in the `read_shapefile`
function.

## Version 1.3 (17 February 2015)
This release adds (a slightly modified version of) Dennis Zwiebler's code for
ignore non-proper streets when reading a shapefile. The API has been changed
for this purpose, in a backwards compatible way:

    std::unique_ptr<Straph> read_shapefile(const std::string& path, unsigned name_idx, bool proper=true);

In the future, this API will be deprecated, as I found out that the name index
can be discovered. The API will then be:

    std::unique_ptr<Straph> read_shapefile(const std::string& path, bool proper=true);

## Version 1.2 (6 February 2015)
This release only contains a small bugfix.

  * Bugfix: `get_route_polylines` is incorrect with test routes, ie. where
    there are no edges.

## Version 1.1 (17 December 2014)
This release has not been extensively tested.

  * New function `combine_routes` which takes multiple routes and merges
    them into a single one. The following functions help make this feature
    useful. Additionally, the routing part of the library takes advantage
    of this, so that the BFS and DFS algorithms effectively route multiple
    paths.
  * New function `get_route_polylines` to retrieve all unique polylines in
    a route, which is useful when combining routes.
  * New function `get_route_junctions` to retrieve all major junctions in
    a route.

