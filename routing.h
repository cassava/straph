/**
 * \file routing.h
 *
 * This header contains classes that are useful when it comes to finding routes
 * within a street graph.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   12. September 2014
 */

#ifndef INCLUDE_GUARD_F5DBBEF2195A44A3919DFC0601352552
#define INCLUDE_GUARD_F5DBBEF2195A44A3919DFC0601352552

#include "straph.h"

#include <memory>   // std::unique_ptr
#include <ostream>  // std::ostream
#include <string>   // std::string
#include <vector>   // std::vector

namespace straph {

/**
 * RouteGenerator is a general interface encompassing all other routing
 * interfaces.
 */
class RouteGenerator {
public:
    virtual ~RouteGenerator() {}

    // initialize must be run before anything else.
    void initialize(const Straph& s) { graph_ = &s; }

    virtual std::unique_ptr<Route> generate_route(const std::vector<Vertex>& points) = 0;
    std::unique_ptr<Route> generate_route(const std::vector<Junction>& points);
    std::unique_ptr<Route> generate_route(const std::vector<std::size_t>& points);

protected:
    const Straph* graph_;
};

/**
 * NoopRouteGenerator is a RouteGenerator that does nothing, hence the name.
 */
class NoopRouteGenerator : public RouteGenerator {
    std::unique_ptr<Route> generate_route(const std::vector<Vertex>& points);
};

/**
 * Router is an interface that lets you route from one vertex to another.
 */
class Router : public RouteGenerator {
public:
    virtual ~Router() {}

    virtual std::unique_ptr<Route> route(Vertex from, Vertex to) = 0;
    virtual std::unique_ptr<Route> generate_route(const std::vector<Vertex>& points);
    std::unique_ptr<Route> route(const Junction& from, const Junction& to);
    std::unique_ptr<Route> route(std::size_t from, std::size_t to);
};

/**
 * BFSRouter routes from one vertex to another via the BFS algorithm.
 *
 * Extra vertices are ignored.
 */
class BFSRouter : public Router {
public:
    std::unique_ptr<Route> route(Vertex from, Vertex to);
};

/**
 * DFSRouter routes from one vertex to another via the DFS algorithm.
 *
 * Extra vertices are ignored.
 */
class DFSRouter : public Router {
public:
    std::unique_ptr<Route> route(Vertex from, Vertex to);
};

/**
 * Generator is an interface that doesn't just route from A to B, but actually
 * only takes the input points as a suggestion.
 *
 * Generators that might make sense are generators that try to make a route
 * have a certain distance, or have a certain curviness.
 */
class Generator : public RouteGenerator {
public:
    virtual ~Generator() {}

    virtual std::unique_ptr<Route> generate() = 0;
    virtual std::unique_ptr<Route> generate(Vertex start) = 0;
    virtual std::unique_ptr<Route> generate_route(const std::vector<Vertex>& points);
    std::unique_ptr<Route> generate(const Junction& start);
    std::unique_ptr<Route> generate(std::size_t start);

};

/**
 * InteractiveGenerator is a route generator that interacts with the user on
 * the command line to find a route.
 *
 * TODO: Not implemented.
 */
class InteractiveGenerator : public Generator {
public:
    std::unique_ptr<Route> generate();
    std::unique_ptr<Route> generate(Vertex start);
};

} // namespace straph
#endif // INCLUDE_GUARD
