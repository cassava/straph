/*
 * read_route.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 12. September 2014
 */

#include "straph.h"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <fstream>
#include <functional>
#include <locale>
#include <string>
using namespace std;

#include <log4cxx/logger.h>

namespace straph {

extern log4cxx::LoggerPtr log; // log is defined in straph.cc

namespace {

inline string& ltrim(string& s) {
        s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
        return s;
}

inline string& rtrim(string& s) {
        s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
        return s;
}

inline string& trim(string& s) {
        return ltrim(rtrim(s));
}

} // anonymous namespace

std::unique_ptr<straph::Route> read_route(const std::string& filepath, double scale)
{
    LOG4CXX_DEBUG(log, "Reading points testdata file " << filepath << " into route...");
    if (scale != 1.0) {
        LOG4CXX_DEBUG(log, "Scaling points by " << scale << "...");
    }

    unique_ptr<Route> route (new Route);
    RouteSegment rseg;
    rseg.name = "Unknown Street";
    Segment seg;

    ifstream in(filepath);
    if (!in.is_open()) {
        LOG4CXX_ERROR(log, "Unable to open route file " << filepath << "!");
        return nullptr;
    }

    unsigned lines = 0, n = 0;
    Segment::count = 0;
    bool new_segment = false;
    Point last_point;

    string line;
    while (getline(in, line)) {
        lines++;
        trim(line);

        // Check if we should have a new segment.
        if (line.length() == 0) {
            if (seg.polyline.size() > 0) {
                rseg.segments.push_back(seg);
                seg = Segment();
                seg.name = rseg.name;
                new_segment = true;
            }
            continue;
        }

        // Check if we should have a new route segment.
        if (line[0] == '#') {
            line = line.substr(1, line.length()-1);
            ltrim(line);
            if (line.length() != 0) {
                if (seg.polyline.size() > 0) {
                    rseg.segments.push_back(seg);
                    seg = Segment();
                    new_segment = true;
                }
                if (rseg.segments.size() > 0) {
                    route->push_back(rseg);
                    rseg = RouteSegment();
                }
                rseg.name = line;
                seg.name = rseg.name;
            }
            continue;
        }

        // If we are here, then we don't have a new segment
        // or route segment, so it must be a point.
        size_t pos = line.find(',');
        if (pos == string::npos) {
            LOG4CXX_WARN(log, "Cannot read point at " << filepath << ":" << lines << "!");
            continue;
        }

        double x, y;
        try {
            x = stod(line.substr(0,pos)) * scale;
            y = stod(line.substr(pos+1,line.length()-(pos+1))) * scale;
        } catch (...) {
            LOG4CXX_WARN(log, "Error reading point at " << filepath << ":" << lines << "!");
            continue;
        }

        if (new_segment) {
            Point cur{x,y};
            if (cur != last_point) {
                LOG4CXX_WARN(log, "Inserting missing connecting point " << last_point << " to " << cur << " in route!");
                seg.polyline.push_back(last_point);
                ++n;
            }
            new_segment = false;
        }

        ++n;
        last_point = Point{x,y};
        seg.polyline.push_back(last_point);
    }

    // Make sure that route has everything before we hand it off.
    if (seg.polyline.size() > 0) {
        rseg.segments.push_back(seg);
    }
    if (rseg.segments.size() > 0) {
        route->push_back(rseg);
    }

    LOG4CXX_INFO(log, "Loaded " << n << " points from " << filepath << " into route with "
        << route->size() << " route segments and " << Segment::count << " segments.");
    return route;
}

} // namespace straph
