/*
 * optimizing.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 30. July 2014
 */

#include "optimizing.h"

#include <boost/graph/graph_traits.hpp>
#include <log4cxx/logger.h>

namespace straph {

// log is defined in straph.cc
extern log4cxx::LoggerPtr log;

/*
 * Iterate through all vertices. For each vertex v, if N(v) == {e1, e2}
 * and each of the edges has the same name, do the following:
 *
 *  1. Get source and destination of the edges -> src, v, dst.
 *  2. Create a new edge from e1 and e2, with src-e1-v-e2-dst order.
 *  3. Insert the new edge and delete e1, e2, and v.
 *
 * This probably is not really that useful, because most streets
 * in our dataset seem to have no names.
 */
void reduce(Straph&)
{
    // TODO
}

} // namespace streetlabel
