/**
 * \file optimizing.h
 *
 * This header contains functions that can be used to optimize graphs.
 * For example, all vertices that have only two incident edges can
 * be merged.
 *
 * For more functions that could be implemented, see TODO.md.
 *
 * TODO: In the name of conformity, should the optimizers be
 * designed and implemented like the routers, all inheriting from
 * a virtual class? It doesn't really seem worth it.
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   30. July 2014
 */

#ifndef INCLUDE_GUARD_C4AFFBAB7F0F429ABF5EDAD0A8514B47
#define INCLUDE_GUARD_C4AFFBAB7F0F429ABF5EDAD0A8514B47

#include "straph.h"

namespace straph {

/**
 * Remove all nodes that contain only two edges and merge the two edges
 * into one edge, thereby reducing the complexity of the graph.
 */
void reduce(Straph& s);

} // namespace straph
#endif // INCLUDE_GUARD
