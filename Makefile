# straph Makefile
#
# This Makefile exists simply to simplify the building
# and testing process. The compilation and installation
# process is managed by CMake.
#
# Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
# 03. September 2014
#

srcdir := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
builddir=build
destdir=~/devel/cc
cmake_flags=-DCMAKE_INSTALL_PREFIX:PATH=${destdir}

.PHONY: all debug release install install-debug install-release uninstall clean test

debug: ${builddir}/debug
	cd $<; \
	cmake -DCMAKE_BUILD_TYPE=debug ${cmake_flags} ${srcdir}; \
	make

release: ${builddir}/release
	cd $<; \
	cmake -DCMAKE_BUILD_TYPE=optimized ${cmake_flags} ${srcdir}; \
	make

test: ${builddir}/debug
	cd $<; \
	cmake -DCMAKE_BUILD_TYPE=debug -DSTRAPH_TEST=ON ${cmake_flags} ${srcdir}; \
	make

all: debug release

install:
	make install-debug
	make install-release

install-debug: debug
	cd ${builddir}/$<; \
	make install

install-release: release
	cd ${builddir}/$<; \
	make install

uninstall:
	@echo CMake does not support uninstall, and since this Makefile is only a proxy
	@echo to CMake, neither do we. The files should have been installed to your
	@echo HOME/devel/c++/{include,lib} folders however. You can delete them there.

clean:
	-rm -r ${builddir}

${builddir}/%:
	mkdir -p $@
