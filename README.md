# Straph

The Straph library has one goal: to read Shapefiles into a street graph and
support operations on that street graph. The name Straph is a portmanteau of
“street” and “graph”, but I shall use the terms interchangeable.

Operations that are currently supported are routing from one vertex to another
and generating routes. The library has been written in an open fashion however,
allowing different algorithms to be added in the future, such as a cleaner for
the read street graph.

In addition, the library supplies the `Point` struct.

There are three headers thus, with increasing inclusion (ie. the last includes
all of the above.)

 * `point.h` contains the Point class with supporting methods and functions.
 * `straph.h` contains the Straph class with IO functions mainly.
 * `routing.h` contains the routing classes for the Straph datatype.

You probably only ever have to include `straph.h` and `routing.h`,
unless you want to use the Point datatype apart from Straph.

## Compilation
Straph should be installed via CMake, which should put the headers into
some `include/straph` directory.

You may need to supply the locations for the Boost and the log4cxx libraries.
This can be done by setting the appropriate variables in the CMake
configuration: `ACTUAL_BOOST_ROOT` and `ACTUAL_LOG4CXX_ROOT`. For each, set
the variable to the path which contains both headers and libraries. For Boost,
you may have to also set the `BOOST_LIBRARYDIR` configuration variable.

In Visual Studio, press F7 to compile, and then build the `INSTALL` target.
You will have to do this once for each configuration (Release and Debug.)
The files will then be installed to the location that you set during CMake
configuration.

## Usage
We assume that you are using the latest C++11 standard.
This is supported by Visual Studio 12 and up.

You start by loading the appropriate classes:

```cpp
#include <memory>
using namespace std;

#include <straph/straph.h>
#include <straph/routing.h>
using namespace straph;
```

and when you are ready to load a shapefile into a Straph datatype, we
need only two data: the filename and the index from which we load the
street names. These names are stored in a `*.dbf` file of the same name
as the `*.shp` file. There are multiple attributes stored for each shape
object, and they are indexed. It is your job (maybe through trial and error?)
to find out which index refers to the name. If you don't, then all streets
might have the same name, which would mess up the way the Straph library works.

```cpp
string path = "../test/unterfranken/roads"; // without the extension is good
uint name_idx = 2; // this is the case with the Unterfranken data
unique_ptr<Straph> s = read_shapefile(path, nameidx);
if (s == nullptr) {
    cerr << "Error: could not read shapefile" << path << ".\n";
    return EXIT_FAILURE;
}
```

When you are ready to route, you can do many things, here is one thing that's
easy to start with:

```cpp
unique_ptr<Router> router{dynamic_cast<Router>(new BFSRouter(*s))};
unique_ptr<Route> sr{router.route(8, 41)};
if (sr == nullptr) {
    cerr << "Error: could not find route from 8 to 41.\n";
    return EXIT_FAILURE;
}
```

That's it. You can then continue doing something with the graph and the route
that you created from it.

## Logging
Straph uses [log4cxx](http://logging.apache.org/log4cxx/) as its logging
framework, which means that you can specify in your own programs exactly
what and how it logs.

The namespace of straph is `straph`.

See the website (of log4cxx) for more information on this.
