# TODOs

## Generators
There are a few more generators that could be implemented. For their support,
a function that would need to be implemented is `travel`, which calculates
not the number of segments, but the effective distance that the polyline
makes up. Are the points close to each other, or far away?

 * `TravelGenerator` uses the travel function to find a route of a particular
    length, be it either minimum or maximum. Constraints could be set during
    the initialization or through functions, which the route function would
    take into consideration.
 * `TwistyGenerator` finds routes where the route twists a lot. This could be
    a subclass of TravelGenerator. We can calculate the angle at each point
    in the polyline to find out whether it is a big angle or not.
 * `SegmentGenerator` just tries to find a route with a certain range of
    segments.
 * `InteractiveGenerator` generates a route by interactively asking the user
    for help. Ask for a starting point. Ask for a connecting point. Route.
    Confirm or discard? Continue, etc. until the user is happy.

## Optimizations
The graph as we get it is pretty ugly. The paths are dirty and not nice and
curvy. This is a good place to tackle that by creating functions to optimize
or clean the street graphs.

 * `CurvifyOptimizer` makes sure that given a certain travel between two
    points, the angles are distributed throughout. This is a refinement of the
    original graph, which does probably make it significantly larger. Plus we
    have to make sure that there are no crossings. This is therefore not a very
    easy optimization.
 * `CleanerOptimizer` tries to eliminate the spontaneous turn-arounds that we
    sometimes see in the data. This would most likely occur by one point being
    behind another one (or on the same point?) which results in the polyline
    tracking back. This should be easier. First thing to do here would be to
    find some real examples in the data.

## Routers
This is not so important as the above two subpoints, but it might be useful to
have the following router elements implemented:

 * `MultiRouter` routes not just from A to B, but also over C, D, E, F, etc.
    Not sure if we would refactor the library or put this in the set up of
    the MultiRouter class.
 * `TravelRouter` finds a route based on the travel characteristic of the
    segments. So instead of just finding the route with the least segments,
    this might find the route with the least distance travelled. Probably
    not as useful for us.
