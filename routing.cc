/*
 * routing.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 12. September 2014
 */

#include "routing.h"

#include <cassert>      // assert
#include <iostream>     // ostream
#include <map>          // map
#include <memory>       // unique_ptr
#include <queue>        // queue
#include <stack>        // stack
#include <string>       // string
using namespace std;

#include <boost/graph/graph_traits.hpp>
#include <log4cxx/logger.h>

namespace straph {

extern log4cxx::LoggerPtr log; // log is defined in straph.cc

namespace {

typedef map<Vertex, Edge> SearchMap;
/*
 * find_vertex is a helper function to find the corresponding Vertex in a graph
 * from the Junction property that is stored in it.
 *
 * This method takes O(n) time.
 */
Vertex find_vertex(const Straph& graph, const Junction& j)
{
    typedef boost::graph_traits<Straph> traits;

    traits::vertex_iterator vi, ve;
    for (tie(vi, ve) = boost::vertices(graph); vi != ve; ++vi) {
        // TODO: Are we comparing contents or addresses?
        if (j == graph[*vi]) {
            return *vi;
        }
    }

    LOG4CXX_DEBUG(log, "Could not find vertex by junction " << j << ".");
    return nullptr;
}

/*
 * find_vertex is a helper function to find the corresponding Vertex in a graph
 * from the index that is stored in the junction property.
 *
 * This method takes O(n) time.
 */
Vertex find_vertex(const Straph& graph, size_t index)
{
    typedef boost::graph_traits<Straph> traits;

    traits::vertex_iterator vi, ve;
    for (tie(vi, ve) = boost::vertices(graph); vi != ve; ++vi) {
        // TODO: Are we comparing contents or addresses?
        if (index == graph[*vi].index) {
            return *vi;
        }
    }

    LOG4CXX_DEBUG(log, "Could not find vertex by index " << index << ".");
    return nullptr;
}

/*
 * bfs searches the graph via BFS if a queue is given,
 * or DFS if a stack is given.
 *
 * Note: graph_search may not actually find end; when all vertices
 * have been looked at it returns the search map. You must check
 * yourself via vm.count(end) if end was found.
 */
SearchMap bfs(const Straph& graph, Vertex start, Vertex end) {
    typedef boost::graph_traits<Straph> traits;

    SearchMap vm;
    queue<Vertex> q;
    set<Vertex> s;
    unsigned loops = 0;

    s.insert(start);
    q.push(start);
    while (!q.empty()) {
        Vertex v = q.front(); q.pop();
        traits::out_edge_iterator ei, ee;
        for (boost::tie(ei, ee) = boost::out_edges(v, graph); ei != ee; ++ei) {
            assert(source(*ei, graph) == v);
            Vertex u = target(*ei, graph);
            if (u == v) {
                loops++;
                continue;
            }

            // Only consider white vertices
            if (!s.count(u)) {
                vm[u] = *ei;        // set edge to parent
                if (u == end) {     // if edge is end, return
                    if (loops > 0) {
                        LOG4CXX_WARN(log, "Found " << loops/2 << " loops in dataset.");
                    }
                    return vm;
                }
                s.insert(u);        // mark black
                q.push(u);          // look at this vertex later
            }
        }
    }

    if (loops > 0) {
        LOG4CXX_WARN(log, "Found " << loops/2 << " loops in dataset.");
    }
    LOG4CXX_WARN(log, "Unable to find route to vertex " << graph[end] << " via BFS.");
    return vm;
}

/*
 * Just a copy of bfs except using a stack instead of a queue.
 */
SearchMap dfs(const Straph& graph, Vertex start, Vertex end) {
    typedef boost::graph_traits<Straph> traits;

    SearchMap vm;
    stack<Vertex> q;
    set<Vertex> s;
    unsigned loops = 0;

    s.insert(start);
    q.push(start);
    while (!q.empty()) {
        Vertex v = q.top(); q.pop();
        traits::out_edge_iterator ei, ee;
        for (boost::tie(ei, ee) = boost::out_edges(v, graph); ei != ee; ++ei) {
            assert(source(*ei, graph) == v);
            Vertex u = target(*ei, graph);
            if (u == v) {
                continue;
            }

            // Only consider white vertices
            if (!s.count(u)) {
                vm[u] = *ei;        // set edge to parent
                if (u == end) {     // if edge is end, return
                    if (loops > 0) {
                        LOG4CXX_WARN(log, "Found " << loops/2 << " loops in dataset.");
                    }
                    return vm;
                }
                s.insert(u);        // mark black
                q.push(u);          // look at this vertex later
            }
        }
    }

    if (loops > 0) {
        LOG4CXX_WARN(log, "Found " << loops/2 << " loops in dataset.");
    }
    LOG4CXX_WARN(log, "Unable to find route to vertex " << graph[end] << " via DFS.");
    return vm;
}

/*
 * compile_search: this is a complicated function that computes the route from a search map.
 */
std::unique_ptr<Route> compile_search(const Straph& graph, Vertex start, Vertex end, SearchMap& vm)
{
    if (vm.count(end) == 0) {
        return nullptr;
    }

    unique_ptr<Route> rp(new Route);
    unsigned nsegs = 0;

    // Initialize the two stacks: after this block, vsta will contain the start
    // node at the front and the end node at the back, and esta will have all
    // the edges between.
    stack<Edge> esta;
    stack<Vertex> vsta;
    for (Vertex v = end, nxt = nullptr; v != start; v = nxt) {
        // All we know is that the vertex v was "found" by edge e.
        // Therefore, v must lie at the front or at the back of e.
        Edge e = vm[v];

        nxt = source(e, graph);
        assert(nxt != v);

        vsta.push(v);
        esta.push(e);
    }
    vsta.push(start);

    RouteSegment rs{};
    while (!esta.empty()) {
        const Junction& j = graph[vsta.top()];
        vsta.pop();
        const Edge e = esta.top();
        esta.pop();

        // We need a copy, because we might need to reverse it.
        Segment s = graph[e];

        LOG4CXX_TRACE(log, "Adding waypoint " << j << " and segment [" << s.front() << "..." << s.back() << "] to route...");

        // If the name is different, then we need to move the old
        // one to rp and create a new RouteSegment.
        if (rs.valid() && rs.name != s.name) {
            rp->push_back(move(rs));
            rs = RouteSegment{};
        }
        rs.name = s.name;

        // If j.coord is not the first point in the segment s, then
        // we have s the wrong way around; we need to reverse it.
        if (j.coord != s.front()) {
            s.reverse();
            assert(j.coord == s.front());
        }

        rs.edges.push_back(e);
        rs.segments.push_back(s);

        nsegs++;
    }
    rp->push_back(move(rs));

    LOG4CXX_DEBUG(log, "Added " << nsegs << " segments to route.");

    return rp;
}

} // anonymous namespace

std::unique_ptr<Route> RouteGenerator::generate_route(const std::vector<Junction>& points)
{
    std::vector<Vertex> vts{};
    for (const auto& elem : points) {
        LOG4CXX_DEBUG(log, "Adding waypoint " << elem << " to route.");
        auto v = find_vertex(*graph_, elem);
        if (v == nullptr) {
            LOG4CXX_ERROR(log, "Could not find vertex with Junction " << elem << "; skipping.");
            continue;
        }
        vts.push_back(v);
    }
    return generate_route(vts);
}

std::unique_ptr<Route> RouteGenerator::generate_route(const std::vector<std::size_t>& points)
{
    std::vector<Vertex> vts{};
    for (const auto& elem : points) {
        LOG4CXX_DEBUG(log, "Adding waypoint " << elem << " to route.");
        auto v = find_vertex(*graph_, elem);
        if (v == nullptr) {
            LOG4CXX_ERROR(log, "Could not find vertex with index " << elem << "; skipping.");
            continue;
        }
        vts.push_back(v);
    }
    return generate_route(vts);
}

std::unique_ptr<Route> NoopRouteGenerator::generate_route(const std::vector<Vertex>& /*points*/)
{
    LOG4CXX_INFO(log, "Routing disabled via NOOP router.");
    return nullptr;
}

/*
 * route takes two Junction objects, finds the corresponding Vertex objects
 * in the graph, and calls the normal (virtual) route method using the two
 * Vertex objects as parameters.
 *
 * If there is an error of any sort, such as no corresponding vertex being
 * found, then a nullptr is returned.
 */
std::unique_ptr<Route> Router::route(const Junction& from, const Junction& to)
{
    LOG4CXX_DEBUG(log, "Routing from junction " << from << " to " << to << ".");
    Vertex from_v = find_vertex(*graph_, from);
    Vertex to_v = find_vertex(*graph_, to);
    return route(from_v, to_v);
}

std::unique_ptr<Route> Router::route(std::size_t from, std::size_t to)
{
    LOG4CXX_DEBUG(log, "Routing from index " << from << " to " << to << ".");
    Vertex from_v = find_vertex(*graph_, from);
    Vertex to_v = find_vertex(*graph_, to);
    return route(from_v, to_v);
}

std::unique_ptr<Route> Router::generate_route(const std::vector<Vertex>& points)
{
    auto n = points.size();
    if (n == 0) {
        LOG4CXX_ERROR(log, "Route waypoints list is empty, need two; aborting.");
        return nullptr;
    } else if (n == 1) {
        LOG4CXX_ERROR(log, "Route waypoints list contains only one waypoint, need two; aborting.");
        return nullptr;
    } else if (n == 2) {
        return route(points[0], points[1]);
    }

    unique_ptr<Route> rte;
    vector<Route*> routes;
    for (size_t i = 0; i < points.size()-1; i++) {
        Route* r = route(points[i], points[i+1]).release();
        if (r == nullptr) {
            LOG4CXX_ERROR(log, "Cannot route between " << (*graph_)[points[i]].index << " and " << (*graph_)[points[i+1]].index << ".");
            goto end;
        }
        routes.push_back(r);
    }

    LOG4CXX_DEBUG(log, "Combining " << n-1 << " routes...");
    rte = combine_routes(routes);

end:
    for (Route* rp : routes) {
        delete rp;
    }
    return rte;
}

std::unique_ptr<Route> BFSRouter::route(Vertex from, Vertex to)
{
    if (from == nullptr || to == nullptr) {
        LOG4CXX_ERROR(log, "One or both of from/to Vertex is null!");
        return nullptr;
    }

    LOG4CXX_DEBUG(log, "Routing via BFSRouter from vertex " << (*graph_)[from] << " to " << (*graph_)[to] << "...");
    SearchMap vm = bfs(*graph_, from, to);
    return compile_search(*graph_, from, to, vm);
}

std::unique_ptr<Route> DFSRouter::route(Vertex from, Vertex to)
{
    if (from == nullptr || to == nullptr) {
        LOG4CXX_ERROR(log, "One or both of from/to Vertex is null!");
        return nullptr;
    }

    LOG4CXX_DEBUG(log, "Routing via DFSRouter from vertex " << (*graph_)[from] << " to " << (*graph_)[to] << "...");
    SearchMap vm = dfs(*graph_, from, to);
    return compile_search(*graph_, from, to, vm);
}

std::unique_ptr<Route> Generator::generate(const Junction& start)
{
    LOG4CXX_DEBUG(log, "Generating route with junction " << start << ".");
    Vertex v = find_vertex(*graph_, start);
    return generate(v);
}

std::unique_ptr<Route> Generator::generate(std::size_t start)
{
    LOG4CXX_DEBUG(log, "Generating route with index " << start << ".");
    Vertex v = find_vertex(*graph_, start);
    return generate(v);
}

std::unique_ptr<Route> Generator::generate_route(const std::vector<Vertex>& points)
{
    auto n = points.size();
    if (n == 0) {
        return generate();
    } else if (n > 1) {
        LOG4CXX_WARN(log, "Ignoring extra waypoints in generator waypoints list.");
    }
    return generate(points[0]);
}

std::unique_ptr<Route> InteractiveGenerator::generate()
{
    // TODO: implement me!
    return nullptr;
}

std::unique_ptr<Route> InteractiveGenerator::generate(Vertex /*start*/)
{
    // TODO: implement me!
    return nullptr;
}

} // namespace straph
