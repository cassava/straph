/**
 * \file point.h
 *
 * This header defines the struct Point along with a whole bunch of
 * methods that pertain to Point types.
 *
 * Note: if you are compiling with Visual Studio, make sure you
 * define _USE_MATH_DEFINES before including cmath or math.h!
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   12. September 2014
 */

#ifndef INCLUDE_GUARD_6D4BD234E1B24366A3CF914E5A4B06AC
#define INCLUDE_GUARD_6D4BD234E1B24366A3CF914E5A4B06AC

#include <cmath>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

namespace straph {

/* Forward declaration */
struct Point;

/**
 * Polyline is a vector of points. We have a typedef to make
 * talking about polylines easier.
 */
typedef std::vector<Point> Polyline;

/**
 * Point is a point in a 2D space.
 */
struct Point {
    Point() : x(0.0), y(0.0) {}
    Point(const Point& p) : x(p.x), y(p.y) {}
    Point(double xa, double ya) : x(xa), y(ya) {}

    /**
     * Return a string representation (x,y) of this Point.
     */
    std::string str() const
    {
        std::ostringstream stream;
        stream << *this;
        return stream.str();
    }

    /**
     * Translate this Point a specific distance along a heading.
     */
    Point& translate(double dist, double heading)
    {
        using std::cos;
        using std::sin;

        x += cos(M_PI_2 - heading) * dist;
        y += sin(M_PI_2 - heading) * dist;
        return *this;
    }

    /**
     * Calculate the distance between this Point and p.
     */
    double distance(const Point& p) const
    {
        using std::sqrt;
        using std::pow;

        Point diff = p - *this;
        return sqrt(pow(diff.x, 2.0) + pow(diff.y, 2.0));
    }

    /**
     * Calculate the area of the rectangle that the coordinates
     * of this Point and p define.
     */
    double area(const Point& p) const
    {
        using std::abs;

        Point diff = p - *this;
        return abs(diff.x * diff.y);
    }

    /**
     * Calculate the heading from this Point to p.
     */
    double heading(const Point& p) const
    {
        using std::atan2;

        Point diff = p - *this;
        double theta = M_PI_2 - atan2(diff.y, diff.x);

        // The result should be in [-pi, +pi).
        if (theta >= M_PI)
            return theta - 2.0*M_PI;
        else if (theta < -M_PI)
            return theta + 2.0*M_PI;
        else
            return theta;
    }

// Operators:
    Point& operator+=(const Point& p) { x+=p.x; y+=p.y; return *this; }
    Point& operator-=(const Point& p) { x-=p.x; y-=p.y; return *this; }
    Point& operator*=(double d) { x*=d; y*=d; return *this; }
    Point& operator/=(double d) { x/=d; y/=d; return *this; }

    friend Point operator+(const Point& lhs, const Point& rhs) { return Point(lhs.x+rhs.x, lhs.y+rhs.y); }
    friend Point operator-(const Point& lhs, const Point& rhs) { return Point(lhs.x-rhs.x, lhs.y-rhs.y); }
    friend Point operator*(const Point& lhs, double rhs) { return Point(lhs.x*rhs, lhs.y*rhs); }
    friend Point operator/(const Point& lhs, double rhs) { return Point(lhs.x/rhs, lhs.y/rhs); }
    friend Point operator*(double lhs, const Point& rhs) { return operator*(rhs, lhs); }

    friend bool operator==(const Point& lhs, const Point& rhs) { return lhs.x==rhs.x && lhs.y==rhs.y; }
    friend bool operator!=(const Point& lhs, const Point& rhs) { return !operator==(lhs, rhs); }
    friend bool operator< (const Point& lhs, const Point& rhs) { return lhs.x<rhs.x || (lhs.x==rhs.x && lhs.y<rhs.y); }
    friend bool operator> (const Point& lhs, const Point& rhs) { return  operator<(lhs, rhs); }
    friend bool operator<=(const Point& lhs, const Point& rhs) { return !operator>(lhs, rhs); }
    friend bool operator>=(const Point& lhs, const Point& rhs) { return !operator<(lhs, rhs); }

    friend std::ostream& operator<<(std::ostream& out, const Point& p)
    {
        return out << "(" << p.x << "," << p.y << ")";
    }

// Data members:
    double x;
    double y;
};

/* Outside operators for Point */
/**
 * Translate the Point p by dist along heading.
 */
Point translate(Point p, double dist, double heading);

/**
 * Calculate the distance between lhs and rhs.
 */
double distance(const Point& lhs, const Point& rhs);

/**
 * Calculate the area of the rectangle defined by lhs and rhs.
 */
double area(const Point& lhs, const Point& rhs);

/**
 * Returns heading of lhs-rhs in radians.
 */
double heading(const Point& lhs, const Point& rhs);

/**
 * Calculate total length of vector of points.
 */
double path_length(const Polyline& path);

/**
 * Calculate middle point in a path.
 */
Point path_middle(const Polyline& path);

std::ostream& operator<<(std::ostream& out, const Polyline& poly);

} // namespace straph
#endif // INCLUDE_GUARD
