Testing
========

The test data in this folder is for use with the program `straph_test`, which
is compiled from `straph_test.cc`. The command line for that tool is for
example:

    ./straph_test unterfranken/roads 2

The second argument gives the index to the names, which is potentially
different for each data set. So here is the list of name indices:

 * unterfranken: 2

