#include "../straph.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

int main(int argc, char **argv)
{
    log4cxx::BasicConfigurator::configure();
    log4cxx::Logger::getRootLogger()->setLevel(log4cxx::Level::getTrace());

    if (argc > 3) {
        printf("Usage: %s <path-to-route> [scale]\n", argv[0]);
        return 1;
    }

    string path = argv[1];
    double scale = 1.0;
    if (argc == 3) {
        try {
            scale = stod(argv[2]);
        } catch (runtime_error err) {
            cerr << err.what() << endl;
            return EXIT_FAILURE;
        }
    }

    auto s = straph::read_route(path, scale);
    cout << "Route " << path << " " << *s;
    cout << "The route is " << (route_valid(*s) ? "valid" : "invalid") << "." << endl;
    return EXIT_SUCCESS;
}
