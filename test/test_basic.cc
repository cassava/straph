#include "../straph.h"
#include "../routing.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

int main(int argc, char **argv)
{
    log4cxx::BasicConfigurator::configure();
    log4cxx::Logger::getRootLogger()->setLevel(log4cxx::Level::getTrace());

    if (argc != 5) {
        printf("Usage: %s <path-to-shapefile> <name-index> <from> <to>\n", argv[0]);
        return 1;
    }

    uint nameidx;
    size_t from, to;
    string path = argv[1];
    try {
        nameidx = stoi(argv[2]);
        from = stoi(argv[3]);
        to = stoi(argv[4]);
    } catch (runtime_error err) {
        cerr << err.what() << endl;
        return EXIT_FAILURE;
    }

    auto s = straph::read_shapefile(path, nameidx);
    //cout << *s << endl;

    auto rter = straph::BFSRouter();
    rter.initialize(*s);
    straph::Router *r = &rter;
    cout << "Routing from 41 -> 8..." << endl;
    auto sr = r->route(from, to);
    if (sr == nullptr) {
        cout << "\t" << "no route found :-(" << endl;
    } else {
        cout << *sr << endl;
    }

    return EXIT_SUCCESS;
}
