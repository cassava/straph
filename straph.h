/**
 * \file straph.h
 *
 * \author Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * \date   02. December 2014
 */

#ifndef INCLUDE_GUARD_780DC44E1E0C4A39BD96D46C2374590B
#define INCLUDE_GUARD_780DC44E1E0C4A39BD96D46C2374590B

#include "point.h"

#include <algorithm>    // std::reverse
#include <memory>       // std::unique_ptr
#include <ostream>      // std::ostream
#include <sstream>      // std::ostringstream
#include <string>       // std::string
#include <vector>       // std::vector

#include <boost/graph/adjacency_list.hpp>

namespace straph {

struct Junction {
    Junction() { index = ++count; }
    Junction(Point p) : coord(p) { index = ++count; }

    std::string str() const
    {
        std::ostringstream stream;
        stream << *this;
        return stream.str();
    }

    void scale(double m)
    {
        coord *= m;
    }

// Operators:
    friend bool operator==(const Junction& lhs, const Junction& rhs)
    {
        return lhs.index==rhs.index && lhs.coord==rhs.coord;
    }

    friend bool operator!=(const Junction& lhs, const Junction& rhs)
    {
        return !operator==(lhs, rhs);
    }

    friend std::ostream& operator<<(std::ostream& out, const Junction& j)
    {
        return out << "{" << j.index << ": " << j.coord << "}";
    }

// Data members:
    std::size_t index;
    Point coord;

    /**
     * WARNING: This count can be reset, which means this class is not thread safe!
     * If you want to count on index being correct and consistent, set it yourself!
     */
    static std::size_t count;
};

struct Segment {
    Segment() { index = ++count; }
    Segment(std::string nam, Polyline poly) : name(nam), polyline(poly) { index = ++count; }

    std::string str() const
    {
        std::ostringstream stream;
        stream << *this;
        return stream.str();
    }

    Point front() const
    {
        return polyline.front();
    }

    Point back() const
    {
        return polyline.back();
    }

    void scale(double m)
    {
        for (Point& p : polyline) {
            p *= m;
        }
    }

    void reverse()
    {
        std::reverse(polyline.begin(), polyline.end());
    }

// Operators:
    friend bool operator==(const Segment& lhs, const Segment& rhs)
    {
        return lhs.index==rhs.index && lhs.name==rhs.name && lhs.polyline==rhs.polyline;
    }

    friend bool operator!=(const Segment& lhs, const Segment& rhs)
    {
        return !operator==(lhs, rhs);
    }

    friend std::ostream& operator<<(std::ostream& out, const Segment& s)
    {
        return out << "{" << s.index << ": \"" << s.name << "\", " << s.polyline << "}";
    }

// Data members:
    std::size_t index;
    std::string name;
    Polyline polyline;

    /**
     * WARNING: This count can be reset, which means this class is not thread safe!
     * If you want to count on index being correct and consistent, set it yourself!
     */
    static std::size_t count;
};

/**
 * Straph is an undirected graph modelling junctions as vertices and street (segments)
 * as edges.
 *
 * Implementation details: we use the adjacency list as the underlying representation,
 * because each vertex will have 2.5 connecting edges on average, which makes it a very
 * sparse graph.
 */
typedef boost::adjacency_list<boost::vecS, boost::setS, boost::undirectedS, Junction, Segment> Straph;

/**
 * Vertex is likely a pointer to a Junction.
 */
typedef Straph::vertex_descriptor Vertex;

/**
 * Edge is a likely a pointer to a Segment.
 */
typedef Straph::edge_descriptor Edge;

/**
 * Read a Shapefile into a Straph.
 *
 * The argument path must to to a shapefile, with or without the extension.
 * Without is preferred.
 *
 * The argument name_idx refers to the position in the dbf file where the
 * name of the street is to be retrieved.
 *
 * Note: this function cannot read all shapefiles, only roads.{shp,shx,dbf}.
 *
 * The returned Straph needs to be checked for validity, in case of error.
 * No exceptions are thrown.
 */
std::unique_ptr<Straph> read_shapefile(const std::string& path, unsigned name_idx, bool proper=true);

/**
 * From each value in the graph subtract the minimum over all values.
 * This leads to better results when converting the double values to
 * floats later, at the cost of losing information.
 */
void normalize_straph(Straph& graph);

/**
 * Scale the street graph.
 */
void scale_straph(Straph& graph, double scalar);

/**
 * Prints a representation (containing newlines) of a Straph s to out,
 * such as:
 *
 *  {
 *      Edges: [
 *          {1: Name, [(0,0), (1,2), ..., (56,34)]}
 *          ...
 *      ]
 *      Vertices: [
 *          {1: (0,0)}: 1, 5, 23
 *          ...
 *      ]
 *  }
 */
std::ostream& operator<<(std::ostream& out, const Straph& s);

/**
 * A RouteSegment is a pair of (vertices, name) representing a stretch of
 * road that belongs to one name. We label each RouteSegment differently.
 * Note that a RouteSegment may be made up of multiple Segments.
 */
struct RouteSegment {
    RouteSegment() {}

    bool valid() const
    {
        return !edges.empty() && !segments.empty() && name != "";
    }

    std::size_t size() const
    {
        std::size_t sum{};
        for (const auto& s : segments) {
            sum += s.polyline.size();
        }
        return sum;
    }

    std::string str() const
    {
        std::ostringstream stream;
        stream << *this;
        return stream.str();
    }

    void set_name(const std::string& n) {
        name = n;
        for (Segment& s : segments) {
            s.name = n;
        }
    }

// Operators:
    friend std::ostream& operator<<(std::ostream& out, const RouteSegment& p);

// Data members:
    std::string name;
    std::vector<Segment> segments;
    std::vector<Edge> edges;
};

/**
 * A Route is a vector of RouteSegments, this contains the entire route
 * from ONE start to ONE end.
 */
typedef std::vector<RouteSegment> Route;

/**
 * Reads a custom route from the file and scales it if scale != 1.0.
 */
std::unique_ptr<Route> read_route(const std::string& filepath, double scale);

/**
 * Combines a vector of routes into a single route.
 * If routes cannot be combined, then return nullptr.
 *
 * Note: We could return the route up till that point,
 * but as it is a programming error, we want it to be obvious.
 */
std::unique_ptr<Route> combine_routes(const std::vector<Route*>& list);

/**
 * Returns a vector of unique polylines in the Route, not in a specific order.
 */
std::vector<Polyline> get_route_polylines(const Route& route);

/**
 * Returns a vector of route points (vertexes, indexes), in order.
 */
std::vector<Junction> get_route_junctions(const Straph& graph, const Route& route);

/**
 * route_valid checks to make sure that route does not have any empty segments
 * or other elements. It returns true if the route is valid.
 *
 * If the route is valid, then testing route.empty() should return a true
 * answer.
 */
bool route_valid(const Route& route);

/**
 * route_rename will remove all unknown street names from the route,
 * replacing them with names of the nearest (last) neighbor street.
 */
void route_rename(Route& route, bool number=true);

/**
 * Prints the Route r to out.
 */
std::ostream& operator<<(std::ostream& out, const Route& r);

} // namespace straph
#endif // INCLUDE_GUARD
