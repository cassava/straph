/*
 * read_straph.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 17. February 2015
 */

#include "straph.h"

#include <cassert>
#include <cstdio>
#include <map>
#include <ostream>
#include <utility>
using namespace std;

#include <boost/graph/graph_traits.hpp>
#include <log4cxx/logger.h>
#include <libshp/shapefil.h>

namespace straph {

extern log4cxx::LoggerPtr log; // log is defined in straph.cc

namespace {

string create_default_name(size_t index)
{
    ostringstream stream;
    stream << "Unknown " << index;
    return stream.str();
}

map<string, bool> type_map{
    {"bridleway", false},
    {"bus_stop", false},
    {"construction", false},
    {"cycleway", false},
    {"desire", false},
    {"dismantled", false},
    {"elevator", false},
    {"footway", false},
    {"living_street", true},
    {"path", false},
    {"pedestrian", false},
    {"platform", false},
    {"primary", true},
    {"primary_link", true},
    {"residential", true},
    {"secondary", true},
    {"secondary_link", true},
    {"service", false},
    {"steps", false},
    {"tertiary", true},
    {"track", false},
    {"trunk", false},
    {"trunk_link", false},
    {"unclassified", false}
};

}

std::unique_ptr<Straph> read_shapefile(const std::string& path, unsigned name_idx, bool proper)
{
    LOG4CXX_DEBUG(log, "Reading shapefile " << path << "...");
    SHPHandle shp = SHPOpen(path.c_str(), "rb");
    DBFHandle dbf = DBFOpen(path.c_str(), "rb");

    Segment::count = 0;
    int entityCount, shapeType;
    SHPGetInfo(shp, &entityCount, &shapeType, NULL, NULL);

    // The file contains only one kind of shape, namely that given by shapeType.
    // Once we have ascertained that the shape type is SHPT_ARC, we only need to
    // check that SHPT_NULL does not show up.
    if (shapeType != SHPT_ARC) {
        LOG4CXX_ERROR(log, "Only shape files of type SHPT_ARC are supported.");

        SHPClose(shp);
        DBFClose(dbf);
        return nullptr;
    }

    int nidx = DBFGetFieldIndex(dbf, "name");
    int tidx = DBFGetFieldIndex(dbf, "type");
    if (nidx != int(name_idx)) {
        LOG4CXX_WARN(log, "name_idx (" << name_idx << ") and name field index (" << nidx << ") are different.");
    }

    unique_ptr<Straph> graph_ptr{new Straph};
    Straph& graph = *graph_ptr;
    map<Point, Vertex> vertexMap;

    // Read in all the Segments and add them to the graph
    size_t unnamed_segments = 0;
    for (int i = 0; i < entityCount; ++i) {
        SHPObject *obj = SHPReadObject(shp, i);
        if (obj->nSHPType == SHPT_NULL) {
            SHPDestroyObject(obj);
            continue;
        }

        string name = DBFReadStringAttribute(dbf, i, nidx);
        if (proper) {
            string type = DBFReadStringAttribute(dbf, i, tidx);
            if (type_map.count(type) == 0) {
                LOG4CXX_ERROR(log, "Unknown type " << type);
                continue;
            } else if (!type_map[type]) {
                continue;
            }

            if (name.empty()) {
                continue;
            }
        }

        // Create new vertices, if they do not already exist.
        //
        // Right now, we are just taking the beginning and ending points of a street segment,
        // but a few lines later we will add all the segments to the edge that spans between
        // the two vertices we (possibly) create right here.
        Point start (obj->padfX[0], obj->padfY[0]);
        Point end (obj->padfX[obj->nVertices-1], obj->padfY[obj->nVertices-1]);
        for (auto p : {start, end}) {
            if (vertexMap.count(p) == 0) {
                Vertex v = add_vertex(graph);
                graph[v].coord = p;
                vertexMap.insert({p, v});
            }
        }

        // Add an edge between the start and end vertices that we just computed.
        bool ok;
        Edge e;
        tie(e, ok) = add_edge(vertexMap[start], vertexMap[end], graph);
        if (!ok) {
            LOG4CXX_FATAL(log, "Unable to add edge to graph (this should NOT happen!)");
        }

        // Fill in all the information on the edge we just created.
        Segment& s = graph[e];
        if (name.empty()) {
            ++unnamed_segments;
            s.name = create_default_name(s.index);
        } else {
            s.name = name;
        }
        for (int j = 0; j < obj->nVertices; ++j) {
            s.polyline.push_back( Point(obj->padfX[j], obj->padfY[j]) );
        }

        assert(graph[source(e, graph)].coord == graph[e].front());
        assert(graph[target(e, graph)].coord == graph[e].back());

        SHPDestroyObject(obj);
    }

    if (unnamed_segments > 0) {
        LOG4CXX_WARN(log, "" << unnamed_segments << " street segments had no names.");
    }

    // Flush and free resources
    SHPClose(shp);
    DBFClose(dbf);
    return graph_ptr;
}

} // namespace straph
