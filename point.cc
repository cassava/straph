/*
 * point.cc
 *
 * Ben Morgan <benjamin.morgan@stud-mail.uni-wuerzburg.de>
 * 12. September 2014
 */

#include "point.h"

#include <cassert>
using namespace std;

namespace straph {

Point translate(Point p, double dist, double heading)
{
    return p.translate(dist, heading);
}

double distance(const Point& lhs, const Point& rhs)
{
    return lhs.distance(rhs);
}

double area(const Point& lhs, const Point& rhs)
{
    return lhs.area(rhs);
}

double heading(const Point& lhs, const Point& rhs)
{
    return lhs.heading(rhs);
}

double path_length(const Polyline& path)
{
    double length = 0.0;
    for (unsigned i = 1; i < path.size(); ++i) {
        length += distance(path[i-1], path[i]);
    }
    return length;
}

Point path_middle(const Polyline& path)
{
    double halfway = path_length(path) / 2.0;
    double current = 0.0;

    assert(halfway > 0.0);

    for (unsigned i = 1; i < path.size(); ++i) {
        current += distance(path[i-1], path[i]);
        if (current > halfway) {
            double d = current - halfway;
            return translate(path[i], d, heading(path[i], path[i-1]));
        }
    }

    // This should not happen:
    return path[0];
}

std::ostream& operator<<(std::ostream& out, const Polyline& poly)
{
    if (poly.empty()) {
        return out << "[]";
    } else if (poly.size() == 1) {
        return out << "[" << poly[0] << "]";
    }

    out << "[" << poly[0];
    for (unsigned i = 1; i < poly.size(); i++) {
        out << ", " << poly[i];
    }
    out << "]";
    return out;
}

} // namespace straph
